extern crate speak_lib;

use speak_lib::TTS;

fn main() {
    let text = std::env::args()
        .nth(1)
        .unwrap_or("Hello World.".into());
    TTS.synth(&text);
    TTS.synchronize();
}
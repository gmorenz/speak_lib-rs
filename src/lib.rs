#[macro_use] extern crate lazy_static;

mod bindings;
use bindings::*;

pub use bindings::espeak_EVENT;
pub use std::os::raw::c_int;

use std::ptr;
use std::ffi::CString;

pub struct Tts {
    sample_rate: u32
}

lazy_static! {
    pub static ref TTS: Tts = unsafe{ Tts::new().expect("failed to initialize espeak") };
}

// I'm going to assume that C has 16 bit shorts... because I doubt espeak even
// works otherwise
pub type SynthCallback = extern "C" fn(samples: *mut i16, len: c_int, events: *mut espeak_EVENT) -> c_int;
pub extern "C" fn default_synth_callback(_samples: *mut i16, _len: c_int, _events: *mut espeak_EVENT) -> c_int {
    return 0; // Continue synthesis
}

pub type UriCallback = extern "C" fn(kind: c_int, uri: *const u8, base: *const u8) -> c_int;
pub extern "C" fn default_uri_callback(_kind: c_int, _uri: *const u8, _base: *const u8) -> c_int {
    return 1; // Speak alternative text instead of playing sound
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Error {
    InternalError,
    BufferFull,
    NotFound
}

impl Error {
    pub fn new(x: c_int) -> Result<(), Error> {
        use espeak_ERROR::*;
        use Error::*;
        match x {
            EE_OK => Ok(()),
            EE_INTERNAL_ERROR => Err(InternalError),
            EE_BUFFER_FULL => Err(BufferFull),
            EE_NOT_FOUND => Err(NotFound),
            _ => panic!("Unknown espeak error")
        }
    }
}

pub use bindings::espeak_PARAMETER as Parameter;

impl Tts {
    unsafe fn new() -> Result<Tts, ()> {
        let sample_rate = espeak_Initialize(
            espeak_AUDIO_OUTPUT::AUDIO_OUTPUT_PLAYBACK,
            60,
            ptr::null(),
            0
        );

        if sample_rate < 0 {
            return Err(())
        }

        let ret = Tts{ sample_rate: sample_rate as u32 };
        ret.set_synth_callback(default_synth_callback);
        ret.set_uri_callback(default_uri_callback);

        Ok(ret)
    }

    /// Note that the callback is called on a different thread.
    pub fn set_synth_callback(&self, callback: SynthCallback) {
        unsafe {
            use std::mem::transmute;
            // Transmute needed to turn callback into something that is unsafe
            // instead of safe, that takes &mut ... instead of *mut ...
            espeak_SetSynthCallback(Some(transmute(callback)));
        }
    }

    pub fn set_uri_callback(&self, callback: UriCallback) {
        unsafe {
            use std::mem::transmute;
            espeak_SetUriCallback(Some(transmute(callback)));
        }
    }

    pub fn synth(&self, text: &str) -> Result<(), Error> {
        let text_c = CString::new(text).unwrap();
        let res = unsafe { espeak_Synth(
            text_c.as_ptr() as *const _,
            text.len() + 1,
            0, // Start position
            espeak_POSITION_TYPE::POS_CHARACTER,
            0, // End position, 0 = none
            0, // Flags, for now none
            ptr::null_mut(), // Unique id... for now none
            ptr::null_mut() // User data... we don't want any
        )};

        Error::new(res)
    }

    pub fn set_parameter(&self, param: Parameter, value: c_int, relative: bool) -> Result<(), Error> {
        let relative_c = if relative { 1 } else { 0 };
        let res = unsafe {
            espeak_SetParameter(param, value, relative_c)
        };

        Error::new(res)
    }

    pub fn parameter(&self, param: Parameter) -> c_int {
        unsafe {
            espeak_GetParameter(param, 1)
        }
    }

    pub fn parameter_default(&self, param: Parameter) -> c_int {
        unsafe {
            espeak_GetParameter(param, 0)
        }
    }

    // TODO: setPonctuationList
    // TODO: Voices

    pub fn cancel(&self) -> Result<(), Error> {
        unsafe{ Error::new(espeak_Cancel()) }
    }

    pub fn is_playing(&self) -> bool {
        1 == unsafe{ espeak_IsPlaying() }
    }

    pub fn synchronize(&self) -> Result<(), Error> {
        unsafe{ Error::new(espeak_Synchronize()) }
    }
}